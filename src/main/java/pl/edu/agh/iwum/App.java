package pl.edu.agh.iwum;

import pl.edu.agh.iwum.predictor.MovementType;
import pl.edu.agh.iwum.predictor.Predictor;

import java.util.*;

public class App {

  public static void main(String[] args) {
    final Map<String, String> classifierData = new HashMap<>();
    classifierData.put("j48", "/home/throzendar/dev/src/IOSR/02/predictor/src/resources/J48_all_motions.model");
    final Predictor predictor = new Predictor();
    predictor.initializeClassifiers(classifierData);

    final List<List<Double>> tests = new ArrayList<>();
    tests.add(Arrays.asList(0.00741, 0.0841, 0.0817));
    tests.add(Arrays.asList(0.00014, 0.0014, 0.014));

    tests.forEach(values -> {
      System.out.println("Cohesion: " + values.get(0) + ", Surface tension: " + values.get(1) + ", Viscosity: " + values.get(2));
      final MovementType result = predictor.test(0.00741, 0.0841, 0.0817);
      System.out.println("Best movement: " + result);
    });
  }
}
