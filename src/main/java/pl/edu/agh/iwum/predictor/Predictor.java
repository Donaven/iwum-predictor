package pl.edu.agh.iwum.predictor;

import weka.classifiers.AbstractClassifier;
import weka.core.Instances;
import weka.core.SerializationHelper;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

public class Predictor {

  private Map<String, AbstractClassifier> classifiers = new HashMap<>();

  public void initializeClassifiers(Map<String, String> modelsData) {
    modelsData.forEach((type, path) -> {
      try {
        final AbstractClassifier classifier = (AbstractClassifier) SerializationHelper.read(new FileInputStream(path));
        if (classifier != null) {
          classifiers.put(type, classifier);
        }
      } catch (Exception e) {
        System.out.println("Could not read model [" + type + "] from file: " + path);
        e.printStackTrace();
      }
    });
  }

  public MovementType test(final double cohesionValue, final double surfaceTensionValue, final double viscosityValue) {
    MovementType result = null;
    double movementClazz = 0;
    for (Map.Entry<String, AbstractClassifier> entry : classifiers.entrySet()) {
      AbstractClassifier classifier = entry.getValue();

      for (MovementType movementType : MovementType.values()) {
        final Instances dataSet = DataSetUtils.createTestDataSet(movementType, cohesionValue, surfaceTensionValue, viscosityValue);
        try {
          final double clazz = classifier.classifyInstance(dataSet.firstInstance());
          System.out.println("[" + movementType + "] Class: " + clazz);
          if (movementClazz < clazz) {
            movementClazz = clazz;
            result = movementType;
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }

    return result;
  }

}
