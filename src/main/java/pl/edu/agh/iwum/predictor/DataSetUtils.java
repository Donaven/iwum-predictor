package pl.edu.agh.iwum.predictor;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

final class DataSetUtils {

  private static final List<String> MOVE_TYPES = Stream.of(MovementType.values())
      .map(MovementType::getTag)
      .collect(Collectors.toList());
  private static final Attribute MOTION = new Attribute("motion", MOVE_TYPES);
  private static final Attribute COHESION = new Attribute("cohesion");
  private static final Attribute SURFACE_TENSION = new Attribute("surface_tension");
  private static final Attribute VISCOSITY = new Attribute("viscosity");
  private static final Attribute RESULT = new Attribute("result", Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"));

  private DataSetUtils() {
  }

  static Instances createTestDataSet(final MovementType movementType, final double cohesion, final double surfaceTension, final double viscosity) {
    final ArrayList<Attribute> attributes = new ArrayList<>(Arrays.asList(MOTION, COHESION, SURFACE_TENSION, VISCOSITY, RESULT));
    final Instances dataSet = new Instances("Test", attributes, 1);
    final DenseInstance instance = new DenseInstance(4);
    instance.setValue(attributes.get(0), movementType.getTag());
    instance.setValue(attributes.get(1), cohesion);
    instance.setValue(attributes.get(2), surfaceTension);
    instance.setValue(attributes.get(3), viscosity);
    dataSet.add(instance);
    dataSet.setClassIndex(dataSet.numAttributes() - 1);
    return dataSet;
  }
}
