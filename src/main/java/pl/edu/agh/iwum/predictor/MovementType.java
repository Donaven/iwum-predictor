package pl.edu.agh.iwum.predictor;

public enum MovementType {

  SIMPLE("simpleMove10"),
  PRECISE("preciseMove10"),
  SHAKING("shakingMove10"),
  RAPID("rapidMove10"),
  FULL_FLIP("fullFlipMove10");

  private final String tag;

  MovementType(String tag) {
    this.tag = tag;
  }

  public String getTag() {
    return tag;
  }
}
